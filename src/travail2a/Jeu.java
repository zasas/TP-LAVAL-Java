/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travail2a;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author zakif
 */
public class Jeu {
    
    public static final int NB_BATEAUX = 4;
    public static final int DIM_X = 4;
    public static final int DIM_Y = 4;
    
    ArrayList<Bateau> listBateaux = new ArrayList<>();

    public Jeu() {
        for(int i =0  ; i < NB_BATEAUX ; i++ ){
         
            Bateau bateau = new Bateau((int) (Math.random() * (DIM_X+1)), (int) (Math.random() * (DIM_Y + 1)));
            listBateaux.add(bateau);
        }
    }
    
    public boolean TousCoules(){
        
        boolean tousCoules = true ;
        for(int i =0 ; i <NB_BATEAUX ; i++ ){
            if(!listBateaux.get(i).EstCoule()){
                tousCoules = false ;
            }
        }
        return tousCoules ;
    }
    
    public void Demarrer(){
        int nbrBateauNonCoule = NB_BATEAUX ;
        boolean touche = false ;
        
        while(!TousCoules()){
        nbrBateauNonCoule = NB_BATEAUX ;  
        if(TousCoules()){
            System.out.println("fini tous coules");
        }else{
            for(int i =0 ; i<NB_BATEAUX ; i++){
                if(listBateaux.get(i).EstCoule()){
                     nbrBateauNonCoule-- ;
                 }  
             }
            System.out.println("nombre de bateau n'est pas encore coule est :"+nbrBateauNonCoule);
            int x = Util.ObtenirNombre(0, DIM_X);
            int y = Util.ObtenirNombre(0, DIM_Y);
            
            for(int i =0 ; i<NB_BATEAUX ; i++){
                if(listBateaux.get(i).EstTouche(x, y)){
                    System.out.println("Un bateau a ete touche");
                    touche = true;
                }
            }
            if(!touche){
                System.out.println("Aucun bateau n’a ete touche");
            }
        }
    }
    }
    
}
