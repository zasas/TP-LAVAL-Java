
package travail2a;

import java.util.Scanner;


public class Util {
    
    public static int ObtenirNombre(int min , int max){
        int nombre ;
        Scanner reader = new Scanner(System.in); 
        do{ 
            System.out.println("Enter a number: ");
            nombre = reader.nextInt();
        }while(nombre < min ||  nombre > max );
        //reader.close(); 
        return nombre ;
    }
}
