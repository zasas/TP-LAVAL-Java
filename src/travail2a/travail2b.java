/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travail2a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author zakif
 */
public class travail2b {
    
   

    public static int minTableau(ArrayList tableauEntier){
        //System.out.println(tableauEntier.size());
        if((int) tableauEntier.size() == 2){
            //System.out.println("aaaa"+tableauEntier);
            if((int) tableauEntier.get(1) < (int) tableauEntier.get(0)){
                return (int) tableauEntier.get(1);
            }else{
                return (int) tableauEntier.get(0);
            }
        }
        else if((int) tableauEntier.size() == 1){
                return (int) tableauEntier.get(0);
        }
        
       
        
        ArrayList tableauDroit = new ArrayList();  
        ArrayList tableauGauche = new ArrayList() ;
        
        
        for(int i=0 ; i<tableauEntier.size() ; i++ ){
            if(i < (tableauEntier.size()/2)){
                tableauDroit.add( (int) tableauEntier.get(i)) ;  
            }else{
                tableauGauche.add((int)tableauEntier.get(i));
            }
        }
        if(minTableau(tableauDroit) < minTableau(tableauGauche)){
           return  minTableau(tableauDroit) ;
        }else{
            return minTableau(tableauGauche);
        }
        
        
    }
    
     public static void main(String[] args) {

         
        ArrayList liste = new ArrayList();
        liste.add(334);
        liste.add(9);
        liste.add(4);
        liste.add(34);
        liste.add(23);
        liste.add(-1);
        liste.add(332);
        liste.add(323);
        liste.add(383);
        
        
        System.out.println("min est :"+minTableau(liste));
    }
}
